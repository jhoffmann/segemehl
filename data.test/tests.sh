#!/bin/bash

SEGEMEHL_DIR=~/segemehl/segemehl_tests
pushd ${SEGEMEHL_DIR}

SIZE=200000
THREADS=8
DATE=$(date +'%F')
TIME=$(date +'%H-%M-%S')
SEGEMEHL=segemehl.x
DATA_DIR=data.test
GIT_BRANCHES=( $(git branch | cut -b 3-) )

updateEnv()
{
  if [ -z $1 ]
  then
    echo 'no testdata'
    exit 1
  fi

  GIT_BRANCH=$(git status -b -s | head -n 1 | cut -f1 -d'.' | cut -f2 -d' ')
  OUTPUT_DIR="output/${DATE}/${GIT_BRANCH}/${TIME}"
  mkdir -p "$OUTPUT_DIR"
  SEGEMEHL_PARAMETER="-S
                      -s
                      -q ${DATA_DIR}/1_${SIZE}.fastq
                      -p ${DATA_DIR}/2_${SIZE}.fastq
                      -i ${DATA_DIR}/Drosophila_melanogaster.BDGP5.13.55.dna.toplevel.idx
                      -d ${DATA_DIR}/Drosophila_melanogaster.BDGP5.13.55.dna.toplevel.fa
                     --threads ${THREADS}
                      -o ${OUTPUT_DIR}/${1}.sm"
}

########################## Existenz Test ######################################
if ! [ -d ${SEGEMEHL_DIR} ]
then
  echo "${SEGEMEHL_DIR} existiert nicht, bitte setzen Sie SEGEMEHL_DIR"
  exit 1
fi

########################## Hauptprogramm ######################################
for branch in $(seq 0 $((${#GIT_BRANCHES[@]} - 1)))
do
  make clean
  git checkout ${GIT_BRANCHES[branch]}
  git pull origin ${GIT_BRANCHES[branch]}
  make segemehl.x -j9
  updateEnv "output_${SIZE}"
  echo ''
  echo "branch: ${GIT_BRANCH}"
  echo " $((${branch} + 1))/${#GIT_BRANCHES[@]}"
  echo ''
  date > "${OUTPUT_DIR}/output_${SIZE}.info" 
  /usr/bin/time -o "${OUTPUT_DIR}/output_${SIZE}.info" \
                -f "%C\n
                    Realtime: %e\n
                    Memoryuse: %K\n
                    CPUusage: %P\n
                    Kerneltime: %S\n
                    Usertime: %U
                    " \
                -a ./${SEGEMEHL} ${SEGEMEHL_PARAMETER}
  date >> "${OUTPUT_DIR}/output_${SIZE}.info" 
  cat "${OUTPUT_DIR}/output_${SIZE}.sm" | sort | tail -n +3 | md5sum >> "${OUTPUT_DIR}/output_${SIZE}.info"
done
popd

echo ''
tail -n +1 ${SEGEMEHL_DIR}/output/${DATE}/*/${TIME}/output_${SIZE}.info
