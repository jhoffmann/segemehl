#!/bin/bash

SEGEMEHL_DIR=~/segemehl/segemehl_tests
pushd ${SEGEMEHL_DIR}

SIZE=200000
THREADS=8
SEGEMEHL=segemehl.x
DATA_DIR=data.test

OUTPUT_DIR="output/oneshot"
mkdir -p "$OUTPUT_DIR"
SEGEMEHL_PARAMETER="-S
	      -s
	      -q ${DATA_DIR}/1_${SIZE}.fastq
	      -p ${DATA_DIR}/2_${SIZE}.fastq
	      -i ${DATA_DIR}/Drosophila_melanogaster.BDGP5.13.55.dna.toplevel.idx
	      -d ${DATA_DIR}/Drosophila_melanogaster.BDGP5.13.55.dna.toplevel.fa
	     --threads ${THREADS}
	      -o ${OUTPUT_DIR}/output_${SIZE}.sm"


########################## Existenz Test ######################################
if ! [ -d ${SEGEMEHL_DIR} ]
then
  echo "${SEGEMEHL_DIR} existiert nicht, bitte setzen Sie SEGEMEHL_DIR"
  exit 1
fi

########################## Hauptprogramm ######################################
make segemehl.x -j9
rm -f ${OUTPUT_DIR}/*
date > "${OUTPUT_DIR}/output_${SIZE}.info" 
/usr/bin/time -o "${OUTPUT_DIR}/output_${SIZE}.info" \
              -f "%C\n
                  Realtime: %e\n
                  Memoryuse: %K\n
                  CPUusage: %P\n
                  Kerneltime: %S\n
                  Usertime: %U
                  " \
              -a ./${SEGEMEHL} ${SEGEMEHL_PARAMETER}
date >> "${OUTPUT_DIR}/output_${SIZE}.info" 
cat "${OUTPUT_DIR}/output_${SIZE}.sm" | sort | tail -n +3 | md5sum | tee -a ${OUTPUT_DIR}/output_${SIZE}.info
echo "43878dd2708497507331068aa49fb82a  <-- soll"
